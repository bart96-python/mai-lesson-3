import os
import sys
import argparse
from datetime import datetime


def create_log(log_filename):
	with open(log_filename, 'w', encoding='utf-8') as f:
		pass


def write_log(text, log_filename):
	if text is not None:
		with open(log_filename, 'a', encoding='utf-8') as f:
			f.write('{} {}'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), text))
			f.write('\n')


# создаем парсер, заводим все нужные нам параметры
# плюсы: ключ -h,--help заводить не нужно, они заведутся по умолчанию и при неправильном вводе пользователя будут работать самостоятельно, как при запуске любой утилиты Windows или Linux
def createParser():
	parser = argparse.ArgumentParser(description='scriptName.py [--digitsCount] [--fullSign] [--partSign] [--logfile]')
	parser.add_argument('--digitsCount', type=int, default=4, help=u"Количество цифр в загаданном числе. Пример: --digits 5. По умолчанию 4 цифры")
	parser.add_argument('--fullSign', type=str, default="B", help=u"Каким символом помечать совпадение по значению и месту. Пример: --fullsign F. По умолчанию B")
	parser.add_argument('--partSign', type=str, default="K", help=u"Каким символом помечать совпадение по месту. Пример: --partialsign P. По умолчанию K")
	parser.add_argument('--logfile', type=str, default="", help=u"Записывать историю ходов в файл (имя файла). Пример: --logfile c:\guessthenumber.log")
	return parser


# переменная, в которую сохраним имя файла лога
logfile_name = ""


def main():
	# создаем объект парсера командной строки
	parser = createParser()
	# получаем список уже разобранных аргументов (за исключением имени скрипта)
	args = parser.parse_args(sys.argv[1:])
	print(type(args))
	# получаем значение ключа "--digits" - из скольких цифр генерить число в игре. остальные параметры обработаете сами
	if args.digits is not None:
		print(f"Количество цифр, заданное пользователем: {args.digits}")
		digits = args.digits
	# создаем файл лога
	if os.path.exists(args.logfile):
		logfile_name = args.logfile
		create_log(logfile_name)

	# как превратить сисок аргументов в словарь:
	args_dict = vars(args)
	print(f"Аргументы: {args_dict}, тип аргументов: {type(args_dict)}")


if __name__ == "__main__":
	# вот так можно посмотреть аргументы командной строки:
	print(f"Системные аргументы: {sys.argv}, тип: {type(sys.argv)}")

	main()
