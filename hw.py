import sys
import json
import random
import argparse
from datetime import datetime


def createParser():
	with open('config.json', 'r', encoding='utf-8') as data:
		config = json.load(data)

	fileName = __file__.split('/')[-1]
	argsList = '] [--'.join(config.keys())
	parser = argparse.ArgumentParser(description=f'{fileName} [--{argsList}]')

	for key, value in config.items():
		helpStr = f"{value['desc']}. Пример: --{key} {value['example']}. По умолчанию {value['default']}"
		parser.add_argument(f'--{key}', default=value['default'], type=int if value['type'] == 'int' else str, help=helpStr)

	return parser


def logger(attemptCount: int, inputValue: str, result: str):
	with open(args.logfile, 'a', encoding='utf-8') as f:
		f.write('{}. {} {} – {}'.format(attemptCount, datetime.now().strftime('%Y-%m-%d %H:%M:%S'), inputValue, result))
		f.write('\n')


def createSecretNumber():
	secretNumber = random.randint(0, float(f'1e{args.digitsCount}') - 1)
	return list(('{:0' + str(args.digitsCount) + 'd}').format(secretNumber))


def isValid(msg: str):
	print(type(args.digitsCount))
	if msg == 'q':
		exit('Game over :(')
	if not msg.isdigit():
		return print('Ошибка: Нужно ввести целое число')
	if len(msg) != args.digitsCount:
		return print(f'Ошибка: Число должно быть {args.digitsCount}-значным')
	else:
		return True


def checkCorrect(attemptCount: list, inputValue: str, secretNumber: list):
	output = [None] * args.digitsCount
	secretNumberClone = secretNumber.copy()

	for index in range(len(output)):
		if inputValue[index] == secretNumber[index]:
			output[index] = args.fullSign
			secretNumberClone[index] = None

	for index in range(len(output)):
		if output[index] is None and inputValue[index] in secretNumberClone:
			output[index] = args.partSign

	print(showResult(attemptCount, inputValue, ''.join(map(str, filter(None, output)))))


def showResult(attemptCount: list, inputValue: str, result: str):
	attemptCount[0] += 1
	logger(attemptCount[0], inputValue, result)

	if result == args.fullSign * args.digitsCount:
		exit(f'Победа! Вы угадали {inputValue} за {attemptCount[0]} раз')
	else:
		return f'{attemptCount[0]}. Вы ввели: {inputValue} Результат: {result or "-"}'


def main():
	attemptCount = [0]
	secretNumber = createSecretNumber()

	while True:
		inputValue = input(f'Введите {args.digitsCount}-значное число или "q" для выхода: ')
		if isValid(inputValue):
			checkCorrect(attemptCount, inputValue, secretNumber)


if __name__ == '__main__':
	argsAll = '\n\t'.join(sys.argv)
	print(f'Код запущен с аргументами: [\n\t{argsAll}\n]\n')

	args = createParser().parse_args(sys.argv[1:])

	main()
